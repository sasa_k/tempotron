import brian2 as brian
ms=brian.ms
mV=brian.mV

indices = brian.array([0, 2, 1])
times = brian.array([1, 6, 14])*brian.ms

encodingth=-64*mV
vth=-60*mV
vrest=-65*mV
vo1 = 2*mV
taum=15*ms
taus=(15/4)*ms
timewindow = 256*ms
nencoding=3
nlearning=3

def plot(Mlearning, Mencoding, Sencoding, MSynapse,SencodingQ):
	fig1 = brian.figure('Learning layer voltage')
	brian.plot(Mlearning.t/brian.ms,Mlearning.v.T/brian.mV)
	fig1.suptitle('Learning layer voltage', fontsize=20)
	fig2 = brian.figure('Encoding layer voltage')
	brian.plot(Mencoding.t/brian.ms,Mencoding.v.T/brian.mV)
	fig2.suptitle('Encoding layer voltage', fontsize=20)
	fig3 = brian.figure('Encoding layer spike')
	brian.plot(Sencoding.t/ms,Sencoding.i,'*')
	fig3.suptitle('Encoding layer spike', fontsize=20)
	fig4 = brian.figure('Synaptic weights')
	brian.plot(MSynapse.t / ms, MSynapse[0].w)
	fig4.suptitle('Synaptic weights', fontsize=20)
	fig5 = brian.figure('Learning layer spike')
	brian.plot(SencodingQ.t/ms,SencodingQ.i,'.')
	fig5.suptitle('Learning layer spike', fontsize=20)

def visualise_connectivity(S):
	Ns = len(S.source)
	Nt = len(S.target)
	brian.figure('Network connectivity',figsize=(10, 4))
	brian.subplot(121)
	brian.plot(brian.zeros(Ns), brian.arange(Ns), 'ok', ms=10)
	brian.plot(brian.ones(Nt), brian.arange(Nt), 'ok', ms=10)
	for i, j in zip(S.i, S.j):
		brian.plot([0, 1], [i, j], '-k')
		brian.xticks([0, 1], ['Source', 'Target'])
		brian.ylabel('Neuron index')
		brian.xlim(-0.1, 1.1)
		brian.ylim(-1, max(Ns, Nt))
		brian.subplot(122)
		brian.plot(S.i, S.j, 'ok')
	brian.xlim(-1, Ns)
	brian.ylim(-1, Nt)
	brian.xlabel('Source neuron index')
	brian.ylabel('Target neuron index')

encoding_model = brian.Equations('''tspike: second
	vo: volt (shared)
	tstart: second (shared)
    	psp= vo*int(tspike >= tstart)*int(tspike <= t)*(exp(-(t-tspike)/taum)-exp(-(t-tspike)/taus)) : volt
	v =vrest + psp : volt''')

synapse_model = '''w : 1
	plastic: boolean (shared)
	psp_post = w*psp_pre : volt (summed)
        didspike = int(vrest + psp_post > vth) : 1
        expected = int(t >= tmax-1)*target_post : 1
        dw = int(plastic)*(expected-didspike)*pconstant*psp_pre : 1
        tmax: second (shared)
        pconstant : 1 (shared)
        nconstant : 1 (shared)'''
        
synapse_learning_spike = '''
    w=w+dw
'''
synapse_encoding_spike = '''
    w=w+dw
'''

    
learning_model = brian.Equations('''
			psp : volt
			v = vrest + psp: volt
			target: 1''')


def learn(encoding_layer, learning_layer, synapse, data, target):
	encoding_layer.tspike=data
	encoding_layer.vo = vo1
	learning_layer.target=target
	brian.run(timewindow)

P = brian.NeuronGroup(nencoding,model=encoding_model,threshold='v > encodingth')
P.tspike=times
P.vo = vo1
P.tstart = 0

Q = brian.NeuronGroup(nlearning,model=learning_model,threshold='v > vth')

S=brian.Synapses(P,Q,model=synapse_model,pre=synapse_encoding_spike,post=synapse_learning_spike)
S.connect('i!=j')
S.w=1
S.tmax=timewindow
S.pconstant=0.1
S.nconstant=-S.pconstant
S.plastic = True

print(S.w)
Mlearning = brian.StateMonitor(Q,'v',record=range(nlearning))
Mencoding=brian.StateMonitor(P,'v',record=range(nencoding))
Sencoding = brian.SpikeMonitor(P)
SencodingQ = brian.SpikeMonitor(Q)
MSynapse = brian.StateMonitor(S,'w',record=S[0,:])

for i in range(0,40):
	P.tstart = (i)*timewindow
	S.tmax = (i+1)*timewindow
	learn(P,Q,S,times,brian.ones(nlearning))
	times += timewindow
	
	print('Iteration:#' + str(i))


#visualise_connectivity(S)
plot(Mlearning, Mencoding, Sencoding,MSynapse,SencodingQ)
brian.show()


