class Classifier(object):

	def __init__(self, traindata, iterations = 10):
		self.traindata = traindata
		self.testdata = None

		self.iterations = iterations

		self.temp = Tempotron(len(traindata[0].encoded_data))

		self.classify()

		self.classification = []
		self.TP = 0
		self.FP = 0
		self.TN = 0
		self.FN = 0

	def classify(self):

		i = 0
		while (i < self.iterations):
			for image in self.traindata:
				self.temp.train(image.encoded_data)
			i += 1

	def test(self, testdata):

		self.testdata = testdata

		self.classification = []
		self.TP = 0
		self.FP = 0
		self.TN = 0
		self.FN = 0

		for image in testdata:
			result = self.temp.test(image.encoded_data)
			if (result == 1):
				if (image.digit == self.traindata[0].digit):
					self.TP += 1
				else:
					self.FP += 1
			else:
				if (image.digit == self.traindata[0].digit):
					self.FN += 1
				else:
					self.TN += 1

			self.classification.append(result)
