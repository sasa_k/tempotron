from mnist import MNIST
from MNISTImage import MNISTImage



import numpy as np

def loadMNISTData():
	mndata = MNIST('./python-mnist/data')
	mndata.load_training()
	mndata.load_testing()
	return mndata

def getTrainData(mndata, target, maxsamples):
	train_data = []
	nsamples = 0
	for index,label in enumerate(mndata.train_labels):
		if label in target:
			img=mndata.train_images[index]
			mimage=MNISTImage(label,img,8,256)
			train_data.append(mimage)
			nsamples += 1
		if nsamples >= maxsamples:
			break
	return train_data

mndata = loadMNISTData()
train_data = getTrainData(mndata,[1],100)
print(len(train_data))
print(train_data[0].spike_times)
print(len(train_data[0].spike_times))

#img = mndata.test_images[0]
#mimage = MNISTImage(7,img,8,256)
#encoding = mimage.encoded_data
#for sequence in encoding:
#    if (np.sum(sequence > 0)):
#       print(sequence)
        
#print len(encoding)
# imgBin = np.array([1 if x > 0 else 0 for x in img])
# B = np.reshape(imgBin, (-1,28))
#import matplotlib
#matplotlib.use("TkAgg")
# import matplotlib.pyplot as plt
# plt.imshow(B)
# plt.show()
