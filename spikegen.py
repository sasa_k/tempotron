from random import randrange
 
def knuth_shuffle(x):
    for i in range(len(x)-1, 0, -1):
        j = randrange(i + 1)
        x[i], x[j] = x[j], x[i]


def getSpikeTime (data, indices, pixelPerNeuron,encoding='linear'):
    if encoding == 'random':
        knuth_shuffle(indices)
    spiketimes = []
    sampledindices = []
    for i in range(0,len(data),pixelPerNeuron):
        spiketime = int("".join(map(str,[data[indices[j]] for j in range(i,i+pixelPerNeuron)])),2)
	#spiketime = map(str,[data[indices[j]] for j in range(i,i+pixelPerNeuron)]
        spiketimes.append(spiketime)
        sampledindices.append([indices[j] for j in range(i,i+pixelPerNeuron)])
    
    return spiketimes, sampledindices
    
    
data = [0,0,1,0,1,0,0,1,1,1,0,0,1,0,1]
indices = range(0,len(data))
timewindow=8
pixelPerNeuron = 3
spiketimes,sampledindices = getSpikeTime(data,indices,pixelPerNeuron,'linear')
print(spiketimes)
print(sampledindices)

#indices = np.array([0, 1, 2])
#times = np.array([3,1,4])*ms
#G=SpikeGeneratorGroup(3,indices,times)
