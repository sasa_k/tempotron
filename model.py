import brian2 as brian
ms=brian.ms

model_str = """w:1
	vo: volt
	tmax: second
"""
v_th=-60*brian.mV
v_rest=-65*brian.mV
taum=15*ms
taus=(15/4)*ms
pre_eq_str='v += w*vo*(exp(-(tmax-t)/taum)-exp(-(tmax-t)/taus))'
#pre_eq_str='v_post += w*v_pre'
synapse_model = '''w : 1
		vtot_post = w*psp_pre : volt (summed)
		#vo : volt
		tmax: second'''
pkernel = brian.Equations('''ti: second
	vo: volt
    psp= vo*int(ti <= t)*(exp(-(t-ti)/taum)-exp(-(t-ti)/taus)) : volt
	v =v_rest + psp : volt''')

#pre_eq_str ='v+=w'

#eqs = brian.Equations('''target: 1
#			vtot : volt
#			v = vtot : volt
#			dv/dt = (-1/taum + 1/taus)*v : volt''')
eqs = brian.Equations('''
			vtot : volt
			v = v_rest + vtot: volt
			target: 1''')
indices = brian.array([0, 2, 1])
times = brian.array([1, 6, 14])*brian.ms

test=brian.NeuronGroup(3,model=pkernel,threshold='t==ti')
test.ti=brian.array([1, 6, 14])*ms
test.vo=2*brian.mV
P = test#brian.SpikeGeneratorGroup(3,indices,times)
Q = brian.NeuronGroup(3,model=eqs,threshold='v > v_th')
#S=brian.Synapses(P,Q,model=model_str,pre=pre_eq_str)
S=brian.Synapses(P,Q,model=synapse_model)
S.connect(True)
#S.connect('i==j')
#Q.vtot=v_rest
S.w=1#'rand()'
#S.vo = 2.15*brian.mV
S.tmax=16*brian.ms
M = brian.StateMonitor(Q,'v',record=range(3))
M2=brian.StateMonitor(test,'v',record=range(3))
sm = brian.SpikeMonitor(test)
brian.run(50*brian.ms)
#print(Q.v)
print(Q.target)
print(S.w)
print(S.t)
#print(S.vo)
test.ti=(brian.array(range(1,4))+20)*ms
S.tmax += 5*brian.ms
#S.w='rand()'
brian.run(4*brian.ms)
#print(Q.v)
print(Q.target)
print(S.w)
print(S.tmax)
brian.plot(M.t/brian.ms,M.v.T/brian.mV)
brian.figure()
brian.plot(M2.t/brian.ms,M2.v.T/brian.mV)
brian.figure()
brian.plot(sm.t/ms,sm.i,'*')
brian.show()
#M.plot(refresh=1*brian*ms, showlast=500*brian.ms)
#print(S.I)
