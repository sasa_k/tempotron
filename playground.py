

import brian2 as brian
import numpy as np
ms=brian.ms
mV=brian.mV
sec=brian.second
nS = brian.nS

indices = np.array([0,2,1])
times = np.array([1,2,3])*ms

G1 = brian.SpikeGeneratorGroup(3,indices,times)

N = 4
tau = 10*ms
vr = -70*mV
vt= -60*mV

eqs='''
dv/dt = -v/tau : volt
'''

eqs2='''
dv/dt = (1/tau_m - 1/tau_s)*v : volt
'''
tmax=16
tau_m=15*ms
tau_s=(15/4)*ms

G2 = brian.NeuronGroup(N,eqs,threshold='v>vt',reset='v=vr')
S1 = brian.Synapses(G1,G2, model='w : volt', pre='v+=w')
'''
@check_units(vr=brian.mV,tmax=ms,t=ms)
def K(vr,tmax,t):
	dt=(tmax-t)/ms
	return vr*(exp(-dt/tau_m) - exp(-dt/tau_s))
'''
#G3 = brian.NeuronGroup(N,eqs,threshold="v>vt", reset='v=vr')
membrane_eq= brian.Equations('v = 2*(exp(-(tmax-t)/tau_m)-exp(-(tmax-t)/tau_s)) : volt')
G3 = brian.NeuronGroup(N,eqs,threshold="v>vt")

S2 = brian.Synapses(G2,G3, model='w:volt', pre='v+=w')

S1.connect(True)
S2.connect('i==j')

inputMonitor = brian.SpikeMonitor(G1)
outputMonitor = brian.SpikeMonitor(G2)

targetValues = np.array([1,0,0,0])
timewindow = 4
dt=0
learning_rate = 0.001
tau_m=15
tau_s = 15/4
V0 = 2.15
def K(tmax, ti):
	dt=tmax-ti
	return V0*(np.exp(-dt/tau_m) - np.exp(-dt/tau_s))

tol = 0.5
for i in range(0,timewindow):
	brian.run(1*ms)
	#print "input spikes: ", inputMonitor.spike_trains()
	#print "output spikes: ", outputMonitor.spike_trains()
	#print "target values: ", targetValues
	dt = dt+1
	outputspikes= outputMonitor.spike_trains()
	for j in range(0,3):
		for k in range(0,N):
			didspike = 0
			if outputspikes[k].size > 1 and outputspikes[k][0]-dt*ms > 0*ms and outputspikes[k][0]-dt*ms < tol*ms:
				didspike = 1
			if (didspike == 1 and targetValues[k] == 0):
				S1.w[j,k] += learning_rate*K(timewindow,dt)
			elif (didspike == 0 and targetValues[k] == 1):
				S1.w[j,k] -= learning_rate*K(timewindow,dt)
			  	
'''
brian.plot(outputMonitor.t/ms,outputMonitor.i,'.')
brian.show()
brian.plot(inputMonitor.t/ms,inputMonitor.i,'*')
brian.show()
'''
print(G2)
print(S2)
